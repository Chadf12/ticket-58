#!/bin/bash

# Output file
OUTPUT_FILE="/tmp/serverinfo.info"

# Function to get the current date and time
get_date() {
    echo "Current Date and Time: $(date)"
}

# Function to get the last 10 users who logged into the server
get_last_10_users() {
    echo "Last 10 Users Who Logged In:"
    last -n 10 | head -n 10
}

# Function to get the swap space usage
get_swap_space() {
    echo "Swap Space Usage:"
    free -h | grep -i swap
}

# Function to get the kernel version
get_kernel_version() {
    echo "Kernel Version: $(uname -r)"
}

# Function to get the IP address
get_ip_address() {
    echo "IP Address:"
    hostname -I | awk '{print $1}'
}

# Main script execution
{
    echo "Gathering server information..."

    get_date
    echo

    get_last_10_users
    echo

    get_swap_space
    echo

    get_kernel_version
    echo

    get_ip_address
    echo

    echo "Server information gathering completed."
} > "$OUTPUT_FILE"

echo "Server information has been saved to $OUTPUT_FILE"

